package hu.plajko.challenge2021;

import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class Day03 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day03.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var nums = AdventUtils.readLines("day03.in").stream()
            .map(l -> Arrays.asList(l.split(" +")).stream()
                .filter(s -> !s.equals(""))
                .map(Integer::parseInt)
                .collect(Collectors.toList()))
            .collect(Collectors.toList());
        List<List<List<Integer>>> partitions = Lists.partition(nums, 3);
        var solution = Lists.partition(nums, 3).stream()
            .flatMap(p -> Stream.of(
                List.of(p.get(0).get(0),p.get(1).get(0),p.get(2).get(0)),
                List.of(p.get(0).get(1),p.get(1).get(1),p.get(2).get(1)),
                List.of(p.get(0).get(2),p.get(1).get(2),p.get(2).get(2))))
            .filter(n -> check(n)).count();
        System.out.println(solution);
    }

    private static boolean check(List<Integer> nums) {
        return nums.get(0) + nums.get(1) > nums.get(2) &&
            nums.get(0) + nums.get(2) > nums.get(1) &&
            nums.get(1) + nums.get(2) > nums.get(0);
    }

}
