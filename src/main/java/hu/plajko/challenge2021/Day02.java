package hu.plajko.challenge2021;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.Map;

@Slf4j
public class Day02 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day02.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var dirs = Map.of(
            'U', new int[] {0,-1},
            'D', new int[] {0, 1},
            'L', new int[] {-1, 0},
            'R', new int[] {1, 0});
        char[][] pad = new char[][] {
            new char[] {0,    0,  '1',  0,   0},
            new char[] {0,   '2', '3', '4',  0},
            new char[] {'5', '6', '7', '8', '9'},
            new char[] {0,   'A', 'B', 'C',  0},
            new char[] {0,    0,  'D',  0,   0},
        };
        int x = 0;
        int y = 2;
         var lines = AdventUtils.readLines("day02.in");
         for(var l: lines) {
             for(int i=0; i<l.length();i++) {
                 var d = dirs.get(l.charAt(i));
                 var nx = x + d[0];
                 var ny = y + d[1];
                 if(nx >= 0 && nx <= 4) {
                     if(pad[y][nx] != 0) {

                         x = nx;
                     }
                 }
                 if(ny >= 0 && ny <= 4) {
                     if(pad[ny][x] != 0) {
                         y = ny;
                     }
                 }
             }
             System.out.println( pad[y][x]);
         }
    }

}
