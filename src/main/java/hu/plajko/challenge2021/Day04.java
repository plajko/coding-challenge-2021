package hu.plajko.challenge2021;

import com.google.common.collect.Ordering;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public class Day04 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day04.class, args);
    }

    @Value
    static class Data {
        String code;
        int id;
        String checkSum;

        boolean valid() {
            return checkSum.equals(checkSum());
        }

        String checkSum() {
            var chars = code.chars().filter(i -> i != '-').boxed().collect(Collectors.toList());
            var counts = chars.stream().collect(Collectors.groupingBy(c -> c, Collectors.counting()));
            var candidates = new ArrayList<>(counts.entrySet());
            candidates.sort(Ordering.natural().<Entry<Integer, Long>>onResultOf(e -> e.getValue()).reverse()
                .thenComparing(Ordering.natural().onResultOf(e -> e.getKey())));
            return candidates.subList(0, 5).stream()
                .map(e->e.getKey()).map(i -> String.valueOf((char)i.intValue())).collect(Collectors.joining());
        }

        String decode() {
            int mod = 'z' - 'a' + 1;
            return code.chars()
                       .map(c -> c == '-' ? ' ' : ('a' + ((c - 'a' + id) % mod)))
                       .mapToObj(i -> String.valueOf((char) i))
                       .collect(Collectors.joining());
        }
    }

    @Override
    public void run(String... args) throws Exception {
        Pattern p = Pattern.compile("^(.*)-(\\d+)\\[(.*)\\]$");
        var nums = AdventUtils.readLines("day04.in").stream()
            .map(p::matcher)
            .filter(m -> m.matches())
            .map(m -> new Data(m.group(1), Integer.parseInt(m.group(2)), m.group(3)))
            .collect(Collectors.toList());
        System.out.println(nums.stream().filter(n -> n.valid()).mapToInt(r -> r.getId()).sum());
        System.out.println(nums.stream().filter(n -> n.decode().contains("pole")).findFirst());
        System.out.println(new Data("qzmt-zixmtkozy-ivhz", 343, "").decode());
    }

}
