package hu.plajko.challenge2021;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
public class Day01 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day01.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
         var l = Arrays.asList(AdventUtils.readLines("day01.in").get(0).split(", *"));
         System.out.println(l);
         List<int[]> dir = new ArrayList<>(List.of(
             new int[] { 0, 1 },
             new int[] { -1, 0 },
             new int[] { 0, -1 },
             new int[] { 1, 0 }));
         Set<List<Integer>> locations = new HashSet<>();
         int x = 0;
         int y = 0;
         a: for (String step: l) {
             int distance = Integer.parseInt(step.substring(1));
             if(step.startsWith("R")) {
                 Collections.rotate(dir, 1);
             } else {
                 Collections.rotate(dir, -1);
             }
             int xp = dir.get(0)[0] * distance;
             int yp = dir.get(0)[1] * distance;
             var steps = new ArrayList<Integer>();
             for (int xi = x; xi != x + xp; xi += dir.get(0)[0]) {
                 steps.add(xi);
                 if(!locations.add(List.of(xi, y))) {
                     x = xi;
                     break a;
                 }
             }
             for (int yi = y; yi != y + yp; yi += dir.get(0)[1]) {
                 steps.add(yi);
                 if(!locations.add(List.of(x, yi))) {
                     y = yi;
                     break a;
                 }
             }
             x += xp;
             y += yp;
             System.out.println(step + " - " + x + ":" + y + " - " + steps);
         }
         System.out.println(x + " - "+  y);
         System.out.println(Math.abs(x) + Math.abs(y));
    }

}
