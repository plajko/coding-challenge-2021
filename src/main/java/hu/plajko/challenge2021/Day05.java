package hu.plajko.challenge2021;

import com.google.common.hash.Hashing;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
public class Day05 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day05.class, args);
    }

    @Value
    static class Letter {
        int pos;
        String s;
    }

    @Override
    public void run(String... args) throws Exception {
        var md5 = Hashing.md5();
        var check = new HashSet<Integer>();
        var letters = IntStream.iterate(0, i -> i + 1)
            .mapToObj(i -> md5.hashString("ojvtpuvg" + i, StandardCharsets.UTF_8).toString())
            .filter(s -> s.startsWith("00000"))
            .map(s -> new Letter(!s.substring(5,6).matches("\\d+") ? 9 : Integer.parseInt(s.substring(5,6)), s.substring(6,7)))
                .filter(l -> l.getPos() < 8)
                .filter(l -> check.add(l.getPos()))
            .limit(8)
            .collect(Collectors.toList());
        var pass = new String[8];
        letters.forEach(l -> pass[l.getPos()] = l.getS());
        System.out.println(Arrays.stream(pass).collect(Collectors.joining()));
    }

}
