package hu.plajko.challenge2021;

import com.google.common.collect.Ordering;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
public class Day06 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day06.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        var lines = AdventUtils.readLines("day06.in");
        var counts = IntStream.range(0, 8)
         .mapToObj(i -> lines.stream()
             .map(l -> String.valueOf(l.charAt(i)))
             .collect(Collectors.groupingBy(c -> c, Collectors.counting())).entrySet().stream().sorted(Ordering.natural().onResultOf(e -> e.getValue())).findFirst())
         .map(e -> e.get().getKey())
         .collect(Collectors.joining());
        System.out.println(counts);

    }

}
