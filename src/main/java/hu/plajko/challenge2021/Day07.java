package hu.plajko.challenge2021;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public class Day07 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day07.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var lines = AdventUtils.readLines("day07.in");

        System.out.println(lines.stream().filter(l -> supports2(l)).count());
        System.out.println(supports2("[mnop]aaaa"));
    }

    static boolean supports(String s) {
        Pattern p = Pattern.compile("\\[(.*?)\\]");
        var m = p.matcher(s);
        var normal = new ArrayList<String>();
        var bracket = new ArrayList<String>();
        int last = 0;
        while (m.find()) {
            normal.add(s.substring(last, m.start()));
            bracket.add(s.substring(m.start() + 1, m.end() - 1));
            last = m.end();
        }
        if (last < s.length()) {
            normal.add(s.substring(last));
        }
        return normal.stream().anyMatch(ss -> hasABBA(ss)) && bracket.stream().noneMatch(ss -> hasABBA(ss));
    }

    static String reverse(String s) {
        return List.of(s.charAt(1), s.charAt(0), s.charAt(1)).stream().map(String::valueOf).collect(Collectors.joining());
    }

    static boolean supports2(String s) {
        Pattern p = Pattern.compile("\\[(.*?)\\]");
        var m = p.matcher(s);
        var normal = new ArrayList<String>();
        var bracket = new ArrayList<String>();
        int last = 0;
        while (m.find()) {
            normal.add(s.substring(last, m.start()));
            bracket.add(s.substring(m.start() + 1, m.end() - 1));
            last = m.end();
        }
        if (last < s.length()) {
            normal.add(s.substring(last));
        }
        var normalAba = normal.stream().flatMap(n -> ABA(n).stream()).collect(Collectors.toSet());
        var bracketAba = bracket.stream().flatMap(n -> ABA(n).stream().map(ss->reverse(ss))).collect(Collectors.toSet());
        return bracketAba.stream().anyMatch(normalAba::contains);
    }

    static Set<String> ABA(String s) {
        int i = 0;
        var result = new HashSet<String>();
        while (i <= s.length() - 3) {
            if (s.charAt(i) != s.charAt(i + 1) &&  s.charAt(i) == s.charAt(i + 2)) {
                result.add(s.substring(i,i+3));
            }
            i++;
        }
        return result;
    }

    static boolean hasABBA(String s) {
        int i = 0;
        while (i <= s.length() - 4) {
            if (s.charAt(i) != s.charAt(i + 1) &&  new StringBuilder(s.substring(i + 0, i + 2)).reverse().toString().equals(s.substring(i + 2, i + 4))) {
                return true;
            }
            i++;
        }
        return false;
    }

}
